function BIE2D_BSS

clear all
close all

%maxNumCompThreads(1);
display(maxNumCompThreads, 'Number of threads used')

DRIVER_scaling


return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function DRIVER_scaling

rng('default')
rng(0)

% Set the global parameters.
nbox_max  = 64;
ntot_vec  = nbox_max*(2.^(9));
TBUILD    = zeros(length(ntot_vec),2);
TSOLVE    = zeros(length(ntot_vec),2);
TCOMP     = zeros(length(ntot_vec),1);
MEM       = zeros(length(ntot_vec),2);
ERR       = zeros(length(ntot_vec),2);
ERRCOMP   = zeros(1,length(ntot_vec));
acc       = 1e-10;
nsamp     = 10;
flag_geom = 'star';
%flag_pot  = 'dr';
flag_pot  = 'ht';
kh        = 100;


for i = 1:length(ntot_vec)
    
  ntot = ntot_vec(i);
  fprintf(1,'ntot = %d\n',ntot)

  % Set the geometry:
  [C,curvelen] = LOCAL_construct_contour(ntot,flag_geom);

  % Set the problem:
  params       = [curvelen,kh,curvelen/ntot];

  % Compress the matrix using either brute force or proxy sources.
  %NODES = LOCAL_compress_HBS_nsym_brute(C,nbox_max,acc,flag_pot,params);
  tic
  Ae = LOCAL_compress_HBS_nsym_green(C,nbox_max,1e-14,flag_pot,params);
  TCOMP(i) = toc;
  tmp = whos('Ae');
  fprintf(1,'Time = %8.3f sec    Mem = %0.2f MB\n',TCOMP(i),tmp.bytes/(2^20))
 
  % Validate the compression.
  q          = randn(ntot,3);
  u          = LOCAL_apply_HBS(Ae,q);
  [~,J]      = sort(rand(1,ntot));
  J          = J(1:nsamp);
  u_exact    = LOCAL_applyrows(C,flag_pot,params,J,q);
  ERRCOMP(i) = norm(u(J,:) - u_exact, 'fro')/norm(u_exact, 'fro')
  
  
  tic
  NODES = LOCAL_compress_HBS_nsym_green(C,nbox_max,acc,flag_pot,params);
  TCOMP(i) = toc;
  tmp = whos('NODES');
  fprintf(1,'Time = %8.3f sec    Mem = %0.2f MB\n',TCOMP(i),tmp.bytes/(2^20))
  %LOCAL_print_average_ranks(NODES)    

  u          = LOCAL_apply_HBS(NODES,q);
  ERRCOMP(i) = norm(u(J,:) - u_exact, 'fro')/norm(u_exact, 'fro')  

  tic
  NODES = LOCAL_compress_HBS_nsym_green_rank(C,NODES,flag_pot,params);
  TCOMP(i) = toc;
  tmp = whos('NODES');
  fprintf(1,'Time = %8.3f sec    Mem = %0.2f MB\n',TCOMP(i),tmp.bytes/(2^20))
  LOCAL_print_average_ranks(NODES)

  u          = LOCAL_apply_HBS(NODES,q);
  ERRCOMP(i) = norm(u(J,:) - u_exact, 'fro')/norm(u_exact, 'fro')
  
  
  if isreal(u_exact)
      bb      = randn(ntot,1);
  else
      bb      = complex(randn(ntot,1),randn(ntot,1));
  end
  
  % Solve using the HBS block sparse representation.
  toplevel = 1;
  tic
  [AAA,klevel] = LOCAL_blocksparse(NODES,toplevel,C,flag_pot,params);
  t = toc;
  display(t, 'Block sparse assemble time') 

  
  tic
  %[L,U,ind,q]    = lu(AAA,'vec');
  [L,U,ind]    = lu(AAA,'vec');
  TBUILD(i,1)  = toc;
  bb_big       = [bb;zeros(size(AAA,1)-ntot,size(bb,2),'like',bb)];
  tic
  xx_big       = U\(L\bb_big(ind,:));
  TSOLVE(i,1)  = toc;
  if isreal(AAA)
    MEM(i,1)   = (nnz(L)+nnz(U))*12;
  else
    MEM(i,1)   = (nnz(L)+nnz(U))*20;
  end
  Axx         = LOCAL_apply_HBS(Ae, xx_big(1:ntot,:));
  ERR(i,1)    = norm(bb-Axx, 'fro')/norm(bb, 'fro');  
  

  display(TCOMP','Compression')
  display(TBUILD(i,:), 'Build')
  display(TSOLVE(i,:), 'Solve')
  display(MEM(i,:)/1e9, 'Mem (GB)')
  display(ERR(i,:), 'Err')
  
  
  nboxes = size(NODES,2);
  L    = NODES{2,nboxes};
  name = ['bie_',flag_pot,'_acc',num2str(-log10(acc)),'/L',num2str(L),'_hbs.bin']
  tic
  write_sparse_matrix(AAA, name);
  toc
  
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The contour C is encoded as follows: C(1,i)  ->  g1(  t(i))
%                                      C(2,i)  ->  g1'( t(i))
%                                      C(3,i)  ->  g1''(t(i))
%                                      C(4,i)  ->  g2(  t(i))
%                                      C(5,i)  ->  g2'( t(i))
%                                      C(6,i)  ->  g2''(t(i))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [C,curvelen] = LOCAL_construct_contour(ntot,flag_geom)

r        = 0.3;
k        = 3;

tt       = linspace(0,2*pi*(1 - 1/ntot),ntot);
C        = zeros(6,ntot );
C(1,:)   =   1.5*cos(tt) + (r/2)*            cos((k+1)*tt) + (r/2)*            cos((k-1)*tt);
C(2,:)   = - 1.5*sin(tt) - (r/2)*(k+1)*      sin((k+1)*tt) - (r/2)*(k-1)*      sin((k-1)*tt);
C(3,:)   = - 1.5*cos(tt) - (r/2)*(k+1)*(k+1)*cos((k+1)*tt) - (r/2)*(k-1)*(k-1)*cos((k-1)*tt);
C(4,:)   =       sin(tt) + (r/2)*            sin((k+1)*tt) - (r/2)*            sin((k-1)*tt);
C(5,:)   =       cos(tt) + (r/2)*(k+1)*      cos((k+1)*tt) - (r/2)*(k-1)*      cos((k-1)*tt);
C(6,:)   = -     sin(tt) - (r/2)*(k+1)*(k+1)*sin((k+1)*tt) + (r/2)*(k-1)*(k-1)*sin((k-1)*tt);
curvelen = 2*pi;

% figure
% plot(C(1,:),C(4,:),'LineWidth',3)
% set(gca,'fontsize',22);
% 
% c=[0, 0.4470, 0.7410];
% text(-0.95,0,'\Gamma','FontSize',35)
% text(1.5,1.2,'\Omega','FontSize',40)
% 
% saveas(gcf,'contour','epsc')

%keyboard

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function NODES = LOCAL_get_tree(C,nbox_max)

ntot = size(C,2);

NODES_0 = cell(10,ntot);

% Construct the top node.
NODES_0{1,1} = [1,ntot];
NODES_0{2,1} = 0;
NODES_0{3,1} = -1;
NODES_0{4,1} = [];
NODES_0{5,1} = 0;
NODES_0{6,1} = 1;
NODES_0{7,1} = ntot;
NODES_0{8,1} = -1; 
NODES_0{9,1} = -1;

% Create smaller nodes via hierarchical subdivision.
% We sweep one level at a time.
ibox_last =  0;
ncreated  =  1;
while (ncreated > 0)
  ibox_first = ibox_last + 1;
  ibox_last  = ibox_last + ncreated;
  ncreated   = 0;
  for ibox = ibox_first:ibox_last
    nbox = NODES_0{7,ibox};
    if (nbox > nbox_max)
      nhalf              = floor(NODES_0{7,ibox}/2);
      if (nhalf > 0)
        ison1            = ibox_last + ncreated + 1;
        NODES_0{4,ibox}  = [NODES_0{4,ibox},ison1];   % Add a child to ibox.
        NODES_0{5,ibox}  = NODES_0{5,ibox}+1;         % Add a child to ibox.
        NODES_0{2,ison1} = NODES_0{2,ibox}+1;         % Set the level           of the child.
        NODES_0{3,ison1} = ibox;                      % Set the parent          of the child.
        NODES_0{4,ison1} = [];                        % Set the child list      of the child.
        NODES_0{5,ison1} = 0;                         % Set the child list      of the child.
        NODES_0{6,ison1} = NODES_0{6,ibox};           % Set the left-most index of the child. 
        NODES_0{7,ison1} = nhalf;                     % Set the number of nodes of the child.
        ncreated         = ncreated + 1;
      end
      if (nhalf < NODES_0{7,ibox})
        ison2            = ibox_last + ncreated + 1;  
        NODES_0{4,ibox}  = [NODES_0{4,ibox},ison2];   % Add a child to ibox.
        NODES_0{5,ibox}  = NODES_0{5,ibox}+1;         % Add a child to ibox. 
        NODES_0{2,ison2} = NODES_0{2,ibox}+1;         % Set the level           of the child.   
        NODES_0{3,ison2} = ibox;                      % Set the parent          of the child.
        NODES_0{4,ison2} = [];                        % Set the child list      of the child. 
        NODES_0{5,ison2} = 0;                         % Set the child list      of the child.   
        NODES_0{6,ison2} = NODES_0{6,ibox} + nhalf;   % Set the left-most index of the child.
        NODES_0{7,ison2} = NODES_0{7,ibox} - nhalf;   % Set the number of nodes of the child. 
        ncreated = ncreated + 1;
      end
    end
  end
end

% This whole section is devoted to the simple task of
% decreasing the size of NODES_0.
% The operation NODES_0 = NODES_0{:,1:nboxes} apparently
% does not work for "cell" structures.
nboxes = ibox_last;
NODES  = cell(50,nboxes);
for ibox = 1:nboxes
  for j = 1:size(NODES_0,1)
    NODES{j,ibox} = NODES_0{j,ibox};
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function uu = LOCAL_apply_HBS(NODES,qq)

nboxes = size(NODES,2);
FIELDS = cell(2,nboxes);

% Initialize the fields on the leaves
%  - Construct the outgoing fields.
%  - Set the incoming fields to zero.
for ibox = nboxes:(-1):2
  if (NODES{5,ibox}==0)
    ind            = NODES{6,ibox} - 1 + (1:NODES{7,ibox});
    FIELDS{1,ibox} = LOCAL_skel_proj(NODES{30,ibox},NODES{31,ibox},qq(ind,:));
    FIELDS{2,ibox} = zeros(length(NODES{22,ibox}),size(qq,2));
  end
end

% Construct all outgoing potentials:
for ibox = nboxes:(-1):2
  if (NODES{5,ibox}>0) % ibox has at least one son.
    if (NODES{5,ibox}==2) % ibox has two sons.
      ison1 = NODES{4,ibox}(1);
      ison2 = NODES{4,ibox}(2);
      qloc  = [FIELDS{1,ison1};FIELDS{1,ison2}];
    else
      ison  = NODES{4,ibox}(1);
      qloc  = FIELDS{1,ison};
    end
    FIELDS{1,ibox} = LOCAL_skel_proj(NODES{30,ibox},NODES{31,ibox},qloc);
  end
end

% Construct all incoming potentials:
% First construct the incoming potentials for the top sibling pair.
FIELDS{2,2} = NODES{46,2}*FIELDS{1,3};
FIELDS{2,3} = NODES{46,3}*FIELDS{1,2};
% Then proceed with all subordinate sibling pairs.
for ibox = 2:nboxes
  if (NODES{5,ibox}==2) % ibox has two sons
    ison1   = NODES{4,ibox}(1);
    ison2   = NODES{4,ibox}(2);
    n1      = length(NODES{22,ison1});
    n2      = length(NODES{22,ison2});
    u_long  = LOCAL_skel_eval(NODES{32,ibox},NODES{33,ibox},FIELDS{2,ibox});
    FIELDS{2,ison1} = u_long(1:n1,:)        + NODES{46,ison1}*FIELDS{1,ison2};
    FIELDS{2,ison2} = u_long(n1 + (1:n2),:) + NODES{46,ison2}*FIELDS{1,ison1};
  elseif (NODES{5,ibox}==1) % ibox has one son
    ison           = NODES{4,ibox}(1);
    FIELDS{2,ison} = LOCAL_skel_eval(NODES{32,ibox},NODES{33,ibox},FIELDS{2,ibox});
  end
end

% Construct the potential on all the leaves:
uu = zeros(size(qq));
for ibox = nboxes:(-1):2
  if (NODES{5,ibox}==0)
    ind       = NODES{6,ibox} - 1 + (1:NODES{7,ibox});
    uu(ind,:) = LOCAL_skel_eval(NODES{32,ibox},NODES{33,ibox},FIELDS{2,ibox}) + ...
                NODES{40,ibox}*qq(ind,:);
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function qq_proj = LOCAL_skel_proj(T,J,qq)

if (min(size(T)) == 0)
  qq_proj = qq(J,:);
else
  k       = size(T,1);
  qq_proj = qq(J(1:k),:) + T*qq(J((k+1):end),:);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function uu_eval = LOCAL_skel_eval(T,J,uu)

uu_eval = zeros(length(J),size(uu,2));
if (min(size(T)) == 0)
  uu_eval(J,:) = uu;
else
  k                       = size(T,1);
  uu_eval(J(1:k),:)       = uu;
  uu_eval(J((k+1):end),:) = T'*uu;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function A = LOCAL_construct_A_diag(C,ind,flag_pot,params)

curvelen = params(1);
kh       = params(2);
ntot     = size(C,2);
h        = curvelen/ntot;
nloc     = length(ind);

[Y_g1,   X_g1   ] = meshgrid(C(1,ind), C(1,ind));
[Y_g2,   X_g2   ] = meshgrid(C(4,ind), C(4,ind));
[Y_dg1,  X_dg1  ] = meshgrid(C(2,ind), C(2,ind));
[Y_dg2,  X_dg2  ] = meshgrid(C(5,ind), C(5,ind));
[Y_ddg1, X_ddg1 ] = meshgrid(C(3,ind), C(3,ind));
[Y_ddg2, X_ddg2 ] = meshgrid(C(6,ind), C(6,ind));

if (flag_pot == 's2') % This is the single layer pot with 2nd order quadrature.
   den = (X_g1 - Y_g1).^2 + (X_g2 - Y_g2).^2 + eye(length(ind));
   A   = -(h/(4*pi))*log(den).*sqrt(Y_dg1.^2 + Y_dg2.^2);
   MU2 = [ 0.7518812338640025 + 0.1073866830872157e1; ...
          -0.7225370982867850 - 0.6032109664493744];
   D = abs(ind'*ones(1,length(ind)) - ones(length(ind),1)*ind);
   D = abs(mod(D + 2, ntot) - 2);
   [ii1, ii2] = find(D.*(D<3));
   for j = 1:length(ii1)
      i1 = ii1(j);
      i2 = ii2(j);
      d  = D(i1,i2);
      A(i1,i2) = (1 + MU2(d))*A(i1,i2);
   end
elseif (flag_pot == 's6') % This is the single layer pot with 6th order quadrature.
   den = (X_g1 - Y_g1).^2 + (X_g2 - Y_g2).^2 + eye(length(ind));
   A   = -(h/(4*pi))*log(den).*sqrt(Y_dg1.^2 + Y_dg2.^2);
   D = abs(ind'*ones(1,length(ind)) - ones(length(ind),1)*ind);
   D = abs(mod(D + 6, ntot) - 6);
   MU6 = [ 0.2051970990601250e1 + 0.2915391987686505e1;...
          -0.7407035584542865e1 - 0.8797979464048396e1;...
           0.1219590847580216e2 + 0.1365562914252423e2;...
          -0.1064623987147282e2 - 0.1157975479644601e2;...
           0.4799117710681772e1 + 0.5130987287355766e1;...
          -0.8837770983721025   - 0.9342187797694916];
   [ii1, ii2] = find(D.*(D<7));
   for j = 1:length(ii1)
      i1 = ii1(j);
      i2 = ii2(j);
      d  = D(i1,i2);
      A(i1,i2) = (1 + MU6(d))*A(i1,i2);
   end
elseif (flag_pot == 'dr') % This is the "real" double layer potential.
   nn1 = ( Y_dg2./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   nn2 = (-Y_dg1./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   dd  = sqrt((Y_g1 - X_g1).^2 + (Y_g2 - X_g2).^2) + eye(nloc);
   A   = h*((nn1.*(Y_g1 - X_g1) + nn2.*(Y_g2 - X_g2))./dd).*((-1/(2*pi))*(1./dd)).*sqrt(Y_dg1.^2 + Y_dg2.^2);
   for i = 1:nloc
       zg1    = X_g1(  i,i);
       zg2    = X_g2(  i,i);
       zdg1   = X_dg1( i,i);
       zdg2   = X_dg2( i,i);
       zddg1  = X_ddg1(i,i);
       zddg2  = X_ddg2(i,i);
       A(i,i) = h*(-zddg2*zdg1 + zddg1*zdg2)/((4*pi)*(zdg1^2 + zdg2^2)) + 0.5;
   end
   xxc = [0;0];
   A = A + h*(-1/(4*pi))*log((X_g1 - xxc(1)).^2 + (X_g2 - xxc(2)).^2).*sqrt(Y_dg1.^2 + Y_dg2.^2);
elseif (flag_pot == 'ds') % This is the "real" double layer potential + a single layer potential.
   nn1 = ( Y_dg2./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   nn2 = (-Y_dg1./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   dd  = sqrt((Y_g1 - X_g1).^2 + (Y_g2 - X_g2).^2) + eye(nloc);
   A   = h*((nn1.*(Y_g1 - X_g1) + nn2.*(Y_g2 - X_g2))./dd).*((-1/(2*pi))*(1./dd)).*sqrt(Y_dg1.^2 + Y_dg2.^2) + ...
         -(h/(2*pi))*log(dd).*sqrt(Y_dg1.^2 + Y_dg2.^2);
   D = abs(ind'*ones(1,length(ind)) - ones(length(ind),1)*ind);
   D = abs(mod(D + 6, ntot) - 6);
   MU6 = [ 0.2051970990601250e1 + 0.2915391987686505e1;...
          -0.7407035584542865e1 - 0.8797979464048396e1;...
           0.1219590847580216e2 + 0.1365562914252423e2;...
          -0.1064623987147282e2 - 0.1157975479644601e2;...
           0.4799117710681772e1 + 0.5130987287355766e1;...
          -0.8837770983721025   - 0.9342187797694916];
   [ii1, ii2] = find(D.*(D<7));
   for j = 1:length(ii1)
      i1 = ii1(j);
      i2 = ii2(j);
      d  = D(i1,i2);
      A(i1,i2) = (1 + MU6(d))*A(i1,i2);
   end
   for j = 1:nloc
      A(j,j) = 0.5;
   end
   xxc = [0;0];
   A = A + h*(-1/(4*pi))*log((X_g1 - xxc(1)).^2 + (X_g2 - xxc(2)).^2).*sqrt(Y_dg1.^2 + Y_dg2.^2);
elseif (flag_pot == 'h2') % This is the double layer Helmholtz potential with 2nd order quadrature
   nn1 = ( Y_dg2./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   nn2 = (-Y_dg1./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   dd  = sqrt((Y_g1 - X_g1).^2 + (Y_g2 - X_g2).^2) + eye(nloc);
   A   = h*(nn1.*(Y_g1 - X_g1) + nn2.*(Y_g2 - X_g2)).*(1./dd).*(-kh*besselh(1, kh*dd)).*sqrt(Y_dg1.^2 + Y_dg2.^2);
   MU2 = [ 0.7518812338640025 + 0.1073866830872157e1; ...
          -0.7225370982867850 - 0.6032109664493744];
   D = abs(ind'*ones(1,length(ind)) - ones(length(ind),1)*ind);
   D = abs(mod(D + 2, ntot) - 2);
   [ii1, ii2] = find(D.*(D<3));
   for j = 1:length(ii1)
      i1 = ii1(j);
      i2 = ii2(j);
      d  = D(i1,i2);
      A(i1,i2) = (1 + MU2(d))*A(i1,i2);
   end
   A = A - 2*sqrt(-1)*eye(nloc);
elseif (flag_pot == 'h6') % This is the double layer Helmholtz potential with 6th order quadrature
   nn1 = ( Y_dg2./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   nn2 = (-Y_dg1./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   dd  = sqrt((Y_g1 - X_g1).^2 + (Y_g2 - X_g2).^2) + eye(nloc);
   A   = h*(nn1.*(Y_g1 - X_g1) + nn2.*(Y_g2 - X_g2)).*(1./dd).*(-kh*besselh(1, kh*dd)).*sqrt(Y_dg1.^2 + Y_dg2.^2);
   MU6 = [ 0.2051970990601250e1 + 0.2915391987686505e1;...
             -0.7407035584542865e1 - 0.8797979464048396e1;...
              0.1219590847580216e2 + 0.1365562914252423e2;...
             -0.1064623987147282e2 - 0.1157975479644601e2;...
              0.4799117710681772e1 + 0.5130987287355766e1;...
             -0.8837770983721025   - 0.9342187797694916];
   D = abs(ind'*ones(1,length(ind)) - ones(length(ind),1)*ind);
   D = abs(mod(D + 6, ntot) - 6);
   [ii1, ii2] = find(D.*(D<7));
   for j = 1:length(ii1)
      i1 = ii1(j);
      i2 = ii2(j);
      d  = D(i1,i2);
      A(i1,i2) = (1 + MU6(d))*A(i1,i2);
   end
   A = A - 2*sqrt(-1)*eye(nloc);
elseif (flag_pot == 'ht') % Helmoltz kernel:      [double layer pot] + i kh [single layer pot]
   ima = sqrt(-1);
   nn1 = ( Y_dg2./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   nn2 = (-Y_dg1./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   dd  = sqrt((Y_g1 - X_g1).^2 + (Y_g2 - X_g2).^2) + eye(nloc);
   A   = ((nn1.*(Y_g1 - X_g1) + nn2.*(Y_g2 - X_g2)).*(1./dd).*(-kh*besselh(1, kh*dd)) + ...
          ima*kh*besselh(0, kh*dd)).*h.*sqrt(Y_dg1.^2 + Y_dg2.^2);
   MU6 = [ 0.2051970990601250e1 + 0.2915391987686505e1;...
          -0.7407035584542865e1 - 0.8797979464048396e1;...
           0.1219590847580216e2 + 0.1365562914252423e2;...
          -0.1064623987147282e2 - 0.1157975479644601e2;...
           0.4799117710681772e1 + 0.5130987287355766e1;...
          -0.8837770983721025   - 0.9342187797694916];
   D = abs(ind'*ones(1,length(ind)) - ones(length(ind),1)*ind);
   D = abs(mod(D + 6, ntot) - 6);
   [ii1, ii2] = find(D.*(D<7));
   for j = 1:length(ii1)
      i1 = ii1(j);
      i2 = ii2(j);
      d  = D(i1,i2);
      A(i1,i2) = (1 + MU6(d))*A(i1,i2);
   end
   for j = 1:nloc
      A(j,j) = - 2*ima;
   end
elseif (flag_pot == 'da') % This is an artificial double layer pot.
   den = (X_g1 - Y_g1).^2 + (X_g2 - Y_g2).^2 + eye(nloc);
   A = h*(1/(2*pi))*(Y_dg2.*(X_g1 - Y_g1) - Y_dg1.*(X_g2 - Y_g2))./den;
   for i = 1:nloc
       zg1    = X_g1(  i,i);
       zg2    = X_g2(  i,i);
       zdg1   = X_dg1( i,i);
       zdg2   = X_dg2( i,i);
       zddg1  = X_ddg1(i,i);
       zddg2  = X_ddg2(i,i);
       A(i,i) =  h*(-zddg2*zdg1 + zddg1*zdg2)/((4*pi)*(zdg1^2 + zdg2^2));
   end
   A = A + eye(nloc);
elseif (flag_pot == 'sa') % This is an artificial single layer pot.
   den = (X_g1 - Y_g1).^2 + (X_g2 - Y_g2).^2 + eye(nloc);
   A = -(h/(4*pi))*log(den) + h*log(1/h)*eye(nloc);
elseif (flag_pot == 'sn') % This is an artificial non-symmetric single layer pot.
   den = (X_g1 - Y_g1).^2 + (X_g2 - Y_g2).^2 + eye(nloc);
   A = -(h/(4*pi))*log(den).*sqrt(Y_dg1.^2 + Y_dg2.^2) + 10*h*log(1/h)*eye(nloc);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function LOCAL_draw_tree(NODES)

nboxes = size(NODES,2);

figure 
hold off
for ibox = 1:nboxes
  ilevel  = NODES{2,ibox};
  x_left  = NODES{6,ibox};
  x_right = NODES{6,ibox} + NODES{7,ibox} - 1;
  plot([x_left, x_right],ilevel * [    1,   1],'b',...
       [x_left, x_left ],ilevel + [-0.02,0.02],'b',...
       [x_right,x_right],ilevel + [-0.02,0.02],'b')
  text(0.5*(x_left+x_right),ilevel-0.2,sprintf('%3d (%4d)',ibox,NODES{9,ibox}))
  hold on
end
axis ij
hold off

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function LOCAL_print_average_ranks(NODES)

fprintf(1,'====== Computing average ranks:\n')

nboxes  = size(NODES,2);
nlevels = NODES{2,nboxes};

level_list = zeros(1,nlevels);

ilevel = 0;
for ibox = 2:nboxes
  if ~(NODES{2,ibox} == ilevel)
    ilevel = NODES{2,ibox};
    level_list(ilevel) = ibox;
  end
end
  
fprintf(1,'    level     average rank\n')
for ilevel = 1:(nlevels-1)
  nboxes_level = level_list(ilevel+1) - level_list(ilevel);
  ksum = 0;
  for ibox = level_list(ilevel) - 1 + (1:nboxes_level)
    ksum = ksum + NODES{9,ibox};
  end
  fprintf(1,'      %3d          %7.2f\n',ilevel,ksum/nboxes_level)
end
nboxes_level = nboxes - level_list(nlevels) + 1;
ksum = 0;
for ibox = level_list(nlevels) - 1 + (1:nboxes_level)
  ksum = ksum + NODES{9,ibox};
end
fprintf(1,'      %3d          %7.2f\n',nlevels,ksum/nboxes_level)

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function maxRank = LOCAL_get_maximum_ranks(NODES)

nboxes  = size(NODES,2);
nlevels = NODES{2,nboxes};
maxRank = zeros(1,nlevels);

for ibox = 2:nboxes
    l = NODES{2,ibox};
    maxRank(l) = max([maxRank(l), NODES{9,ibox}]);
end

return
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [xxc, R] = LOCAL_get_circum_circle(C)

nloc       = size(C,2);

% First we TRY to create the circle based on the endpoints.
xxc        = 0.5*(C([1,4],1) + C([1,4],end));
distsquare = (C(1,:)-xxc(1)).^2 + (C(4,:)-xxc(2)).^2;
R          = sqrt(max(distsquare));

% The result is an absurdly large circle, then we instead
% base it on the center of mass of the points.
if ( (1.2*R*R) > ( (C(1,1) - C(1,end))^2 + (C(4,1) - C(4,end))^2) )
  xxc        = (1/nloc)*[sum(C(1,:)); sum(C(4,:))];
  distsquare = (C(1,:)-xxc(1)).^2 + (C(4,:)-xxc(2)).^2;
  R          = sqrt(max(distsquare));
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function C = LOCAL_construct_circle(xxc, R, n)

tt = linspace(0, 2*pi*(1-1/n), n);
C  = [xxc(1) + R*cos(tt);...
             - R*sin(tt);...
             - R*cos(tt);...
      xxc(2) + R*sin(tt);...
             + R*cos(tt);...
             - R*sin(tt)];

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function LIST_NEI = LOCAL_construct_potential_neighborlist(C,NODES,radius_rel)

nboxes = size(NODES,2);

% Note that in the construction, we temporarily include
% a box in its own list of neighbors.
LIST_NEI    = cell(1,nboxes);
LIST_NEI{1} = [1];
for ibox = 2:nboxes
  [xxc, R] = LOCAL_get_circum_circle(C(:,NODES{6,ibox}-1+(1:NODES{7,ibox})));
  % Collect of list of all the father's neighbors' sons:
  potneis = [];
  for jbox = LIST_NEI{NODES{3,ibox}}
    potneis = [potneis,NODES{4,jbox}];
  end
  % Loop over all potential neighbors to check which ones are actually "close".
  for jbox = potneis
    ind    = NODES{6,jbox} - 1 + (1:NODES{7,jbox});
    distsq = (C(1,ind)-xxc(1)).^2 + (C(4,ind)-xxc(2)).^2;
    if (min(distsq) < radius_rel*radius_rel*R*R)
      LIST_NEI{ibox} = [LIST_NEI{ibox},jbox];
    end
  end
end
% Finally we remove the box itself from the list of neighbors.
for ibox = 2:nboxes
  j = find(LIST_NEI{ibox} == ibox);
  LIST_NEI{ibox} = sort(LIST_NEI{ibox}([1:(j-1),(j+1):end]));
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function uu = LOCAL_applyrows(C,flag_pot,params,ind_samp,qq)

% if (size(C,2)*length(ind_samp) > 4000000)
%   disp('in LOCAL_applyrows')
%   disp('The problem size seems a bit large ... breaking.')
%   keyboard
% end

ntot   = size(C,2);
indtmp = 1:ntot;
indtmp(ind_samp) = 2*ntot;
indtmp = sort(indtmp);
ind_offd = indtmp(1:(ntot-length(ind_samp)));

A_offd = LOCAL_construct_A_offd(C,ind_samp,ind_offd,flag_pot,params);
A_diag = LOCAL_construct_A_diag(C,ind_samp,flag_pot,params);

uu = A_offd*qq(ind_offd,:) + A_diag*qq(ind_samp,:);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function A = LOCAL_construct_A_offd(C,ind1,ind2,flag_pot,params)

ntot     = size(C,2);
curvelen = params(1);
kh       = params(2);
h        = curvelen/ntot;

addweights = true;

[Y_g1,  X_g1  ] = meshgrid(C(1,ind2), C(1,ind1));
[Y_g2,  X_g2  ] = meshgrid(C(4,ind2), C(4,ind1));
[Y_dg1, X_dg1 ] = meshgrid(C(2,ind2), C(2,ind1));
[Y_dg2, X_dg2 ] = meshgrid(C(5,ind2), C(5,ind1));
%den = (X_g1 - Y_g1).^2 + (X_g2 - Y_g2).^2;

if (flag_pot == 's2') % This is the single layer pot with 2nd order quadrature.
   den = (X_g1 - Y_g1).^2 + (X_g2 - Y_g2).^2;
   A = -(h/(4*pi))*log(den).*sqrt(Y_dg1.^2 + Y_dg2.^2);
   if (addweights)
      MU2 = [ 0.7518812338640025 + 0.1073866830872157e1; ...
             -0.7225370982867850 - 0.6032109664493744];
      D = abs(ind1'*ones(1,length(ind2)) - ones(length(ind1),1)*ind2);
      D = abs(mod(D + 2, ntot) - 2);
      [ii1, ii2] = find(D.*(D<3));
      for j = 1:length(ii1)
         i1 = ii1(j);
         i2 = ii2(j);
         d  = D(i1,i2);
         A(i1,i2) = (1 + MU2(d))*A(i1,i2);
      end
   end
elseif (flag_pot == 's6') % This is the single layer pot with 6th order quadrature.
   den = (X_g1 - Y_g1).^2 + (X_g2 - Y_g2).^2;
   A = -(h/(4*pi))*log(den).*sqrt(Y_dg1.^2 + Y_dg2.^2);
   if (addweights)
      MU6 = [ 0.2051970990601250e1 + 0.2915391987686505e1;...
             -0.7407035584542865e1 - 0.8797979464048396e1;...
              0.1219590847580216e2 + 0.1365562914252423e2;...
             -0.1064623987147282e2 - 0.1157975479644601e2;...
              0.4799117710681772e1 + 0.5130987287355766e1;...
             -0.8837770983721025   - 0.9342187797694916];
      D = abs(ind1'*ones(1,length(ind2)) - ones(length(ind1),1)*ind2);
      D = abs(mod(D + 6, ntot) - 6);
      [ii1, ii2] = find(D.*(D<7));
      for j = 1:length(ii1)
         i1 = ii1(j);
         i2 = ii2(j);
         d  = D(i1,i2);
         A(i1,i2) = (1 + MU6(d))*A(i1,i2);
      end
   end
elseif (flag_pot == 'dr') % This is the "real" double layer potential.
   xxc = [0;0];
   nn1 = ( Y_dg2./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   nn2 = (-Y_dg1./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   dd  = sqrt((Y_g1 - X_g1).^2 + (Y_g2 - X_g2).^2);
   A   = h*((nn1.*(Y_g1 - X_g1) + nn2.*(Y_g2 - X_g2))./dd).*((-1/(2*pi))*(1./dd)).*sqrt(Y_dg1.^2 + Y_dg2.^2) + ...
         h*(-1/(4*pi))*log((X_g1 - xxc(1)).^2 + (X_g2 - xxc(2)).^2).*sqrt(Y_dg1.^2 + Y_dg2.^2);
elseif (flag_pot == 'ds') % This is the "real" double layer potential + a single layer potential.
   xxc = [0;0];
   nn1 = ( Y_dg2./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   nn2 = (-Y_dg1./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   dd  = sqrt((Y_g1 - X_g1).^2 + (Y_g2 - X_g2).^2);
   A   = h*((nn1.*(Y_g1 - X_g1) + nn2.*(Y_g2 - X_g2))./dd).*((-1/(2*pi))*(1./dd)).*sqrt(Y_dg1.^2 + Y_dg2.^2) + ...
         h*(-1/(2*pi))*log(dd).*sqrt(Y_dg1.^2 + Y_dg2.^2);
   if (addweights)
      MU6 = [ 0.2051970990601250e1 + 0.2915391987686505e1;...
             -0.7407035584542865e1 - 0.8797979464048396e1;...
              0.1219590847580216e2 + 0.1365562914252423e2;...
             -0.1064623987147282e2 - 0.1157975479644601e2;...
              0.4799117710681772e1 + 0.5130987287355766e1;...
             -0.8837770983721025   - 0.9342187797694916];
      D = abs(ind1'*ones(1,length(ind2)) - ones(length(ind1),1)*ind2);
      D = abs(mod(D + 6, ntot) - 6);
      [ii1, ii2] = find(D.*(D<7));
      for j = 1:length(ii1)
         i1 = ii1(j);
         i2 = ii2(j);
         d  = D(i1,i2);
         A(i1,i2) = (1 + MU6(d))*A(i1,i2);
      end
   end
   A = A + h*(-1/(4*pi))*log((X_g1 - xxc(1)).^2 + (X_g2 - xxc(2)).^2).*sqrt(Y_dg1.^2 + Y_dg2.^2);
elseif (flag_pot == 'h2') % This is the double layer Helmholtz potential
   nn1 = ( Y_dg2./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   nn2 = (-Y_dg1./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   dd  = sqrt((Y_g1 - X_g1).^2 + (Y_g2 - X_g2).^2);
   A   = h*(nn1.*(Y_g1 - X_g1) + nn2.*(Y_g2 - X_g2)).*(1./dd).*(-kh*besselh(1, kh*dd)).*sqrt(Y_dg1.^2 + Y_dg2.^2);
   if (addweights)
      MU2 = [ 0.7518812338640025 + 0.1073866830872157e1; ...
             -0.7225370982867850 - 0.6032109664493744];
      D = abs(ind1'*ones(1,length(ind2)) - ones(length(ind1),1)*ind2);
      D = abs(mod(D + 2, ntot) - 2);
      [ii1, ii2] = find(D.*(D<3));
      for j = 1:length(ii1)
         i1 = ii1(j);
         i2 = ii2(j);
         d  = D(i1,i2);
         A(i1,i2) = (1 + MU2(d))*A(i1,i2);
      end
   end
elseif (flag_pot == 'h6') % This is the double layer Helmholtz potential
   nn1 = ( Y_dg2./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   nn2 = (-Y_dg1./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   dd  = sqrt((Y_g1 - X_g1).^2 + (Y_g2 - X_g2).^2);
   A   = h*(nn1.*(Y_g1 - X_g1) + nn2.*(Y_g2 - X_g2)).*(1./dd).*(-kh*besselh(1, kh*dd)).*sqrt(Y_dg1.^2 + Y_dg2.^2);
   if (addweights)
      MU6 = [ 0.2051970990601250e1 + 0.2915391987686505e1;...
             -0.7407035584542865e1 - 0.8797979464048396e1;...
              0.1219590847580216e2 + 0.1365562914252423e2;...
             -0.1064623987147282e2 - 0.1157975479644601e2;...
              0.4799117710681772e1 + 0.5130987287355766e1;...
             -0.8837770983721025   - 0.9342187797694916];
      D = abs(ind1'*ones(1,length(ind2)) - ones(length(ind1),1)*ind2);
      D = abs(mod(D + 6, ntot) - 6);
      [ii1, ii2] = find(D.*(D<7));
      for j = 1:length(ii1)
         i1 = ii1(j);
         i2 = ii2(j);
         d  = D(i1,i2);
         A(i1,i2) = (1 + MU6(d))*A(i1,i2);
      end
   end
elseif (flag_pot == 'ht') % Helmholtz kernel:      [double layer pot] + i kh [single layer pot]
   ima = sqrt(-1);
   nn1 = ( Y_dg2./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   nn2 = (-Y_dg1./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   dd  = sqrt((Y_g1 - X_g1).^2 + (Y_g2 - X_g2).^2);
   A   = ((nn1.*(Y_g1 - X_g1) + nn2.*(Y_g2 - X_g2)).*(1./dd).*(-kh*besselh(1, kh*dd)) + ...
          ima*kh*besselh(0, kh*dd)).*h.*sqrt(Y_dg1.^2 + Y_dg2.^2);
   if (addweights)
      MU6 = [ 0.2051970990601250e1 + 0.2915391987686505e1;...
             -0.7407035584542865e1 - 0.8797979464048396e1;...
              0.1219590847580216e2 + 0.1365562914252423e2;...
             -0.1064623987147282e2 - 0.1157975479644601e2;...
              0.4799117710681772e1 + 0.5130987287355766e1;...
             -0.8837770983721025   - 0.9342187797694916];
      D = abs(ind1'*ones(1,length(ind2)) - ones(length(ind1),1)*ind2);
      D = abs(mod(D + 6, ntot) - 6);
      [ii1, ii2] = find(D.*(D<7));
      for j = 1:length(ii1)
         i1 = ii1(j);
         i2 = ii2(j);
         d  = D(i1,i2);
         A(i1,i2) = (1 + MU6(d))*A(i1,i2);
      end
   end
elseif (flag_pot == 'da') % This is an artificial double layer pot.
   A = h*(1/(2*pi))*(Y_dg2.*(X_g1 - Y_g1) - Y_dg1.*(X_g2 - Y_g2))./den;
elseif (flag_pot == 'sa') % This is an artificial single layer pot.
   A = -(h/(4*pi))*log(den);
elseif (flag_pot == 'sn') % This is an artificial non-symmetric single layer pot.
   A = -(h/(4*pi))*log(den).*sqrt(Y_dg1.^2 + Y_dg2.^2);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function A = LOCAL_construct_A_sing(C1,ind1,C2,ind2,flag_pot,params)

kh = params(2);
h  = params(3);

[Y_g1,  X_g1  ] = meshgrid(C2(1,ind2), C1(1,ind1));
[Y_g2,  X_g2  ] = meshgrid(C2(4,ind2), C1(4,ind1));
[Y_dg1, X_dg1 ] = meshgrid(C2(2,ind2), C1(2,ind1));
[Y_dg2, X_dg2 ] = meshgrid(C2(5,ind2), C1(5,ind1));
den             = (X_g1 - Y_g1).^2 + (X_g2 - Y_g2).^2;

if (flag_pot == 's2') % This is the single layer pot with 2nd order quadrature.
   den = (X_g1 - Y_g1).^2 + (X_g2 - Y_g2).^2;
   A = -(h/(4*pi))*log(den).*sqrt(Y_dg1.^2 + Y_dg2.^2);
elseif (flag_pot == 's6') % This is the single layer pot with 6th order quadrature.
   den = (X_g1 - Y_g1).^2 + (X_g2 - Y_g2).^2;
   A = -(h/(4*pi))*log(den).*sqrt(Y_dg1.^2 + Y_dg2.^2);
elseif (flag_pot == 'dr') % This is the "real" double layer potential.
   xxc = [0;0];
   nn1 = ( Y_dg2./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   nn2 = (-Y_dg1./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   dd  = sqrt((Y_g1 - X_g1).^2 + (Y_g2 - X_g2).^2);
   A   = h*((nn1.*(Y_g1 - X_g1) + nn2.*(Y_g2 - X_g2))./dd).*((-1/(2*pi))*(1./dd)).*sqrt(Y_dg1.^2 + Y_dg2.^2) + ...
         h*(-1/(4*pi))*log((X_g1 - xxc(1)).^2 + (X_g2 - xxc(2)).^2).*sqrt(Y_dg1.^2 + Y_dg2.^2);
elseif (flag_pot == 'ds') % This is the "real" double layer potential + a single layer potential.
   xxc = [0;0];
   nn1 = ( Y_dg2./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   nn2 = (-Y_dg1./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   dd  = sqrt((Y_g1 - X_g1).^2 + (Y_g2 - X_g2).^2);
   A   = h*((nn1.*(Y_g1 - X_g1) + nn2.*(Y_g2 - X_g2))./dd).*((-1/(2*pi))*(1./dd)).*sqrt(Y_dg1.^2 + Y_dg2.^2) + ...
         h*(-1/(2*pi))*log(dd).*sqrt(Y_dg1.^2 + Y_dg2.^2);
   A   = A + h*(-1/(4*pi))*log((X_g1 - xxc(1)).^2 + (X_g2 - xxc(2)).^2).*sqrt(Y_dg1.^2 + Y_dg2.^2);
elseif (flag_pot == 'h2') % This is the double layer Helmholtz potential
   nn1 = ( Y_dg2./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   nn2 = (-Y_dg1./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   dd  = sqrt((Y_g1 - X_g1).^2 + (Y_g2 - X_g2).^2);
   A   = h*(nn1.*(Y_g1 - X_g1) + nn2.*(Y_g2 - X_g2)).*(1./dd).*(-kh*besselh(1, kh*dd)).*sqrt(Y_dg1.^2 + Y_dg2.^2);
elseif (flag_pot == 'h6') % This is the double layer Helmholtz potential
   nn1 = ( Y_dg2./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   nn2 = (-Y_dg1./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   dd  = sqrt((Y_g1 - X_g1).^2 + (Y_g2 - X_g2).^2);
   A   = h*(nn1.*(Y_g1 - X_g1) + nn2.*(Y_g2 - X_g2)).*(1./dd).*(-kh*besselh(1, kh*dd)).*sqrt(Y_dg1.^2 + Y_dg2.^2);
elseif (flag_pot == 'ht') % Helmholtz kernel:      [double layer pot] + i kh [single layer pot]
   ima = sqrt(-1);
   nn1 = ( Y_dg2./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   nn2 = (-Y_dg1./sqrt(Y_dg1.*Y_dg1 + Y_dg2.*Y_dg2));
   dd  = sqrt((Y_g1 - X_g1).^2 + (Y_g2 - X_g2).^2);
   A   = ((nn1.*(Y_g1 - X_g1) + nn2.*(Y_g2 - X_g2)).*(1./dd).*(-kh*besselh(1, kh*dd)) + ...
          ima*kh*besselh(0, kh*dd)).*h.*sqrt(Y_dg1.^2 + Y_dg2.^2);
elseif (flag_pot == 'da') % This is an artificial double layer pot.
   A = h*(1/(2*pi))*(Y_dg2.*(X_g1 - Y_g1) - Y_dg1.*(X_g2 - Y_g2))./den;
elseif (flag_pot == 'sa') % This is an artificial single layer pot.
   A = -(h/(4*pi))*log(den);
elseif (flag_pot == 'sn') % This is an artificial non-symmetric single layer pot.
   A = -(h/(4*pi))*log(den).*sqrt(Y_dg1.^2 + Y_dg2.^2);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [T, I] = id_decomp(A,INPUT2)

if (INPUT2 < 1)
  acc = INPUT2;
  [T,I] = skel_col2(A,acc);
else
  k = INPUT2;
  [T,I] = skel_col3(A,k);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [T, I] = skel_col2(A,acc)

ss = svd(A);
k  = sum(ss > acc);
[tmp, R, I] = qr(A,0);

[U,D,V] = svd(R(1:k,1:k));
q = sum(diag(D) > acc);
T = V(:,1:q)*(D(1:q,1:q)\(U(:,1:q)'))*R(1:k, (k+1):size(R,2));

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [T, I] = skel_col3(A,k)

[tmp, R, I] = qr(A,0);
[U,D,V] = svd(R(1:k,1:k));
q = sum(diag(D) > 1e-12);
T = V(:,1:q)*(D(1:q,1:q)\(U(:,1:q)'))*R(1:k, (k+1):size(R,2));

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function NODES = LOCAL_compress_HBS_nsym_green(C,nbox_max,acc,flag_pot,params)

% The following parameters can be tuned:
  % The relative radius of the proxy circle to the cluster, 1.5 is often good.
  radius_rel = 1.75;
  % The number of points on the proxy circle:
  nproxy = 160;
%  if ((flag_pot == 'ht') | (flag_pot == 'h2') | (flag_pot == 'h6'))
%    fprintf(1,'WARNING: For Helmholtz problems, "nproxy" must be tuned to the size of the patch.\n')
%    fprintf(1,'         Make sure the file has been modified to do so.\n')
%  end

% Compute the tree strcture.
NODES  = LOCAL_get_tree(C,nbox_max);
nboxes = size(NODES,2);

% Construct the list of neighbors of any cell.
LIST_NEI = LOCAL_construct_potential_neighborlist(C,NODES,radius_rel);

% Compress all nodes, going from smaller to larger.
for ibox = nboxes:(-1):2
%  fprintf(1,'Processing ibox = %3d at ilevel = %3d\n',ibox,NODES{2,ibox})

% Construct the index vectors for ibox.
  if (NODES{5,ibox}==0) % ibox has no sons.
    indskel_out = NODES{6,ibox} - 1 + (1:NODES{7,ibox});
    indskel_in  = NODES{6,ibox} - 1 + (1:NODES{7,ibox});
  elseif (NODES{5,ibox}==1) % ibox has precisely one son.
    ison1       = NODES{4,ibox}(1);
    indskel_out = NODES{20,ison1};
    indskel_in  = NODES{22,ison1};
  elseif (NODES{5,ibox}==2) % ibox has precisely two sons.
    ison1       = NODES{4,ibox}(1);
    ison2       = NODES{4,ibox}(2);
    indskel_out = [NODES{20,ison1},NODES{20,ison2}];
    indskel_in  = [NODES{22,ison1},NODES{22,ison2}];
  end
  
  % Determine a circle that circumscribes the cell:
  [xxc,R] = LOCAL_get_circum_circle(C(:,sort([indskel_out,indskel_in])));
  Cproxy  = LOCAL_construct_circle(xxc, radius_rel*R, nproxy);
  % Find all contour points inside the circle:
  indskel_in_maybeinside = [];
  indskel_out_maybeinside = [];
  for jbox = LIST_NEI{ibox}
    if (NODES{5,jbox}==0) % The potential neighbor is a leaf.
      indskel_out_maybeinside = [indskel_out_maybeinside,NODES{6,jbox}-1+(1:NODES{7,jbox})];
      indskel_in_maybeinside  = [indskel_in_maybeinside, NODES{6,jbox}-1+(1:NODES{7,jbox})];
    elseif (NODES{5,jbox}==0) % The potential neighbor has precisely one son.
      fprintf(1,'WARNING - option not implemented.\n')
      keyboard
    else
      jbox_son1 = NODES{4,jbox}(1);
      jbox_son2 = NODES{4,jbox}(2);
      indskel_out_maybeinside = [indskel_out_maybeinside,NODES{20,jbox_son1},NODES{20,jbox_son2}];
      indskel_in_maybeinside  = [indskel_in_maybeinside, NODES{22,jbox_son1},NODES{22,jbox_son2}];
    end
  end
  relind = find( ((C(1,indskel_out_maybeinside)-xxc(1)).^2 + ...
                  (C(4,indskel_out_maybeinside)-xxc(2)).^2) < ((radius_rel*R)^2) );
  indskel_out_inside = indskel_out_maybeinside(relind);
  relind = find( ((C(1,indskel_in_maybeinside) -xxc(1)).^2 + ...
                  (C(4,indskel_in_maybeinside) -xxc(2)).^2) < ((radius_rel*R)^2) );
  indskel_in_inside  = indskel_in_maybeinside(relind);

%  plot(C(1,:),C(4,:),'b.',...
%       C(1,indskel_out),C(4,indskel_out),'r.',...
%       C(1,indskel_out_inside),C(4,indskel_out_inside),'g.',...
%       Cproxy(1,:),Cproxy(4,:),'k.')
%  axis equal
%  keyboard
  
  A21proxy = [LOCAL_construct_A_offd(C,     indskel_in_inside,        indskel_out,       flag_pot,params);...
              LOCAL_construct_A_sing(Cproxy,1:nproxy,          C,     indskel_out,       flag_pot,params)];
  A12proxy = [LOCAL_construct_A_offd(C,     indskel_in,               indskel_out_inside,flag_pot,params),...
              LOCAL_construct_A_sing(C,     indskel_in,        Cproxy,1:nproxy,          flag_pot,params)];
   
%  disp(size(A21proxy))
      
  % Compute the skeletons.
  [Tout,Jout] = id_decomp(A21proxy, acc);
  [Tin, Jin ] = id_decomp(A12proxy',acc);
  k = max(size(Tout,1),size(Tin,1));
  % We sometimes need to enforce that the outgoing rank = incoming rank.
  % (Note that this is implemented rather clumsily.)
  if ~(k == size(Tout,1))
    [Tout,Jout] = id_decomp(A21proxy, k);
  elseif ~(k == size(Tin,1))
    [Tin, Jin ] = id_decomp(A12proxy',k);
  end
  % Record the outgoing skeletons:
  NODES{20,ibox} = indskel_out(Jout(1:k));
  NODES{21,ibox} = indskel_out(Jout((k+1):end));
  NODES{30,ibox} = Tout;
  NODES{31,ibox} = Jout;
  % Record the incoming skeletons:
  NODES{22,ibox} = indskel_in(Jin(1:k));
  NODES{23,ibox} = indskel_in(Jin((k+1):end));
  NODES{32,ibox} = Tin;
  NODES{33,ibox} = Jin;
  % Record nskel and kskel
  NODES{ 8,ibox} = length(indskel_out);
  NODES{ 9,ibox} = k;

end

% Construct the matrices representing self-interactions on the leaves.
for ibox = nboxes:(-1):2
  if (NODES{5,ibox}==0) % ibox is a leaf
    ind = NODES{6,ibox} - 1 + (1:NODES{7,ibox});
    NODES{40,ibox} = LOCAL_construct_A_diag(C,ind,flag_pot,params);
  end
end

% Construct the matrices for sibling interactions.
for ibox = nboxes:(-1):1
  if (NODES{5,ibox}==2) % ibox has two sons.
    ison1 = NODES{4,ibox}(1);
    ison2 = NODES{4,ibox}(2);
    NODES{46,ison1} = LOCAL_construct_A_offd(C,NODES{22,ison1},NODES{20,ison2},flag_pot,params);
    NODES{46,ison2} = LOCAL_construct_A_offd(C,NODES{22,ison2},NODES{20,ison1},flag_pot,params);
  end
end

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function NODES = LOCAL_compress_HBS_nsym_green_rank(C,NODES,flag_pot,params)

% The following parameters can be tuned:
  % The relative radius of the proxy circle to the cluster, 1.5 is often good.
  radius_rel = 1.75;
  % The number of points on the proxy circle:
  nproxy = 160;
%  if ((flag_pot == 'ht') | (flag_pot == 'h2') | (flag_pot == 'h6'))
%    fprintf(1,'WARNING: For Helmholtz problems, "nproxy" must be tuned to the size of the patch.\n')
%    fprintf(1,'         Make sure the file has been modified to do so.\n')
%  end

% Compute the tree strcture.
% NODES  = LOCAL_get_tree(C,nbox_max);
nboxes = size(NODES,2);
maxRank = LOCAL_get_maximum_ranks(NODES);

% Construct the list of neighbors of any cell.
LIST_NEI = LOCAL_construct_potential_neighborlist(C,NODES,radius_rel);

% Compress all nodes, going from smaller to larger.
for ibox = nboxes:(-1):2
%  fprintf(1,'Processing ibox = %3d at ilevel = %3d\n',ibox,NODES{2,ibox})

% Construct the index vectors for ibox.
  if (NODES{5,ibox}==0) % ibox has no sons.
    indskel_out = NODES{6,ibox} - 1 + (1:NODES{7,ibox});
    indskel_in  = NODES{6,ibox} - 1 + (1:NODES{7,ibox});
  elseif (NODES{5,ibox}==1) % ibox has precisely one son.
    ison1       = NODES{4,ibox}(1);
    indskel_out = NODES{20,ison1};
    indskel_in  = NODES{22,ison1};
  elseif (NODES{5,ibox}==2) % ibox has precisely two sons.
    ison1       = NODES{4,ibox}(1);
    ison2       = NODES{4,ibox}(2);
    indskel_out = [NODES{20,ison1},NODES{20,ison2}];
    indskel_in  = [NODES{22,ison1},NODES{22,ison2}];
  end
  
  % Determine a circle that circumscribes the cell:
  [xxc,R] = LOCAL_get_circum_circle(C(:,sort([indskel_out,indskel_in])));
  Cproxy  = LOCAL_construct_circle(xxc, radius_rel*R, nproxy);
  % Find all contour points inside the circle:
  indskel_in_maybeinside = [];
  indskel_out_maybeinside = [];
  for jbox = LIST_NEI{ibox}
    if (NODES{5,jbox}==0) % The potential neighbor is a leaf.
      indskel_out_maybeinside = [indskel_out_maybeinside,NODES{6,jbox}-1+(1:NODES{7,jbox})];
      indskel_in_maybeinside  = [indskel_in_maybeinside, NODES{6,jbox}-1+(1:NODES{7,jbox})];
    elseif (NODES{5,jbox}==0) % The potential neighbor has precisely one son.
      fprintf(1,'WARNING - option not implemented.\n')
      keyboard
    else
      jbox_son1 = NODES{4,jbox}(1);
      jbox_son2 = NODES{4,jbox}(2);
      indskel_out_maybeinside = [indskel_out_maybeinside,NODES{20,jbox_son1},NODES{20,jbox_son2}];
      indskel_in_maybeinside  = [indskel_in_maybeinside, NODES{22,jbox_son1},NODES{22,jbox_son2}];
    end
  end
  relind = find( ((C(1,indskel_out_maybeinside)-xxc(1)).^2 + ...
                  (C(4,indskel_out_maybeinside)-xxc(2)).^2) < ((radius_rel*R)^2) );
  indskel_out_inside = indskel_out_maybeinside(relind);
  relind = find( ((C(1,indskel_in_maybeinside) -xxc(1)).^2 + ...
                  (C(4,indskel_in_maybeinside) -xxc(2)).^2) < ((radius_rel*R)^2) );
  indskel_in_inside  = indskel_in_maybeinside(relind);

%  plot(C(1,:),C(4,:),'b.',...
%       C(1,indskel_out),C(4,indskel_out),'r.',...
%       C(1,indskel_out_inside),C(4,indskel_out_inside),'g.',...
%       Cproxy(1,:),Cproxy(4,:),'k.')
%  axis equal
%  keyboard
  
  A21proxy = [LOCAL_construct_A_offd(C,     indskel_in_inside,        indskel_out,       flag_pot,params);...
              LOCAL_construct_A_sing(Cproxy,1:nproxy,          C,     indskel_out,       flag_pot,params)];
  A12proxy = [LOCAL_construct_A_offd(C,     indskel_in,               indskel_out_inside,flag_pot,params),...
              LOCAL_construct_A_sing(C,     indskel_in,        Cproxy,1:nproxy,          flag_pot,params)];
   
%  disp(size(A21proxy))
      
  % Get the target rank
  k = maxRank(NODES{2,ibox});
  %k = max(maxRank);
  
  % Compute the skeletons.
  [Tout,Jout] = id_decomp(A21proxy, k);
  [Tin, Jin ] = id_decomp(A12proxy',k);
  assert(size(Tout,1)==size(Tin,1));

  % Record the outgoing skeletons:
  NODES{20,ibox} = indskel_out(Jout(1:k));
  NODES{21,ibox} = indskel_out(Jout((k+1):end));
  NODES{30,ibox} = Tout;
  NODES{31,ibox} = Jout;
  % Record the incoming skeletons:
  NODES{22,ibox} = indskel_in(Jin(1:k));
  NODES{23,ibox} = indskel_in(Jin((k+1):end));
  NODES{32,ibox} = Tin;
  NODES{33,ibox} = Jin;
  % Record nskel and kskel
  NODES{ 8,ibox} = length(indskel_out);
  NODES{ 9,ibox} = k;

end

% Construct the matrices representing self-interactions on the leaves.
for ibox = nboxes:(-1):2
  if (NODES{5,ibox}==0) % ibox is a leaf
    ind = NODES{6,ibox} - 1 + (1:NODES{7,ibox});
    NODES{40,ibox} = LOCAL_construct_A_diag(C,ind,flag_pot,params);
  end
end

% Construct the matrices for sibling interactions.
for ibox = nboxes:(-1):1
  if (NODES{5,ibox}==2) % ibox has two sons.
    ison1 = NODES{4,ibox}(1);
    ison2 = NODES{4,ibox}(2);
    NODES{46,ison1} = LOCAL_construct_A_offd(C,NODES{22,ison1},NODES{20,ison2},flag_pot,params);
    NODES{46,ison2} = LOCAL_construct_A_offd(C,NODES{22,ison2},NODES{20,ison1},flag_pot,params);
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [A,klevel] = LOCAL_blocksparse(NODES,toplevel,C,flag_pot,params)

nboxes  = size(NODES,2);
ntot    = size(C,2);
nlevels = NODES{2,nboxes};

if (nargin == 2)
  toplevel = 1;
end

%%% Extract the sizes of the block diagonal matrices.
klevel = zeros(1,nlevels);
for ibox = 2:nboxes
  ilevel = NODES{2,ibox};
  klevel(ilevel) = klevel(ilevel) + NODES{9,ibox};
end

%%% Calculate the required memory
M = 0;
for ilevel = nlevels:(-1):toplevel
   M = M + LOCAL_get_blockdiags_memory(NODES,ilevel);
end
%M = M + LOCAL_get_AT_memory(NODES,toplevel);

%%% Assemble the sparse matrix.
nbig = ntot + 2*sum(klevel(toplevel:nlevels));
%AAA = spalloc(nbig, nbig, M);
%AAA  = sparse(nbig,nbig);
%AAA = zeros(M, 3);
IJ = zeros(M, 2);

% determine if value is complex or not
if contains(flag_pot, 'h')
    VAL = zeros(M, 1, 'like', 1j);
else
    VAL = zeros(M, 1);
end

count = 0;
offset = 0;
for ilevel = nlevels:(-1):toplevel
  [IJDD,VALDD,IJUV,VALUV,nloc,kloc] = LOCAL_get_blockdiags(NODES,ilevel,flag_pot);
  
  ind1 = offset + (1:nloc);
  ind2 = offset + nloc + (1:kloc);
  ind3 = offset + nloc + kloc + (1:kloc);
  
%   AAA(ind1,ind1) = DD;
%   AAA(ind1,ind2) = UU;
%   AAA(ind2,ind1) = VV';
%   AAA(ind2,ind3) = -speye(kloc);
%   AAA(ind3,ind2) = -speye(kloc);

    % D
    n = size(IJDD,1);
    IJ(count + (1:n), 1) = offset + IJDD(:, 1);
    IJ(count + (1:n), 2) = offset + IJDD(:, 2);
    VAL(count + (1:n)) = VALDD(:);
    count = count + n;
    
    % V'
    n = size(IJUV,1);
    IJ(count + (1:n), 1) = offset + nloc + IJUV(:, 2);
    IJ(count + (1:n), 2) = offset + IJUV(:, 1);
    VAL(count + (1:n)) = VALUV(:, 2)';
    count = count + n;
    
    % U
    IJ(count + (1:n), 1) = offset + IJUV(:, 1);
    IJ(count + (1:n), 2) = offset + nloc + IJUV(:, 2);
    VAL(count + (1:n)) = VALUV(:, 1);
    count = count + n;
    
    % two identities
    IJ(count + (1:kloc), 1) = ind3;
    IJ(count + (1:kloc), 2) = ind2;
    VAL(count + (1:kloc)) = -ones(kloc,1);
    count = count + kloc;
    
    IJ(count + (1:kloc), 1) = ind2;
    IJ(count + (1:kloc), 2) = ind3;
    VAL(count + (1:kloc)) = -ones(kloc,1);
    count = count + kloc;
    
    offset = offset + nloc + kloc;
end
assert(M == count)

% last block
AT  = LOCAL_get_AT(NODES,toplevel,C,flag_pot,params);

A = sparse(IJ(:,1), IJ(:,2), VAL, nbig, nbig, 2*(M+numel(AT)));
ind = offset + (1:size(AT,1));
A(ind,ind) = AT;


%fprintf('Estimate: %d, true: %d\n', M, nnz(AAA))

%bb = randn(ntot,3);
%xx_exact = A\bb;
%bb_big = [bb;zeros(size(AAA,1)-ntot,size(bb,2))];
%xx_big = AAA\bb_big;
%fprintf(1,'Discrepancy = %10.3e\n',max(max(abs(xx_exact - xx_big(1:ntot,:)))))
%keyboard
return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [IJDD,VALDD,IJUV,VALUV,nloc,kloc] = LOCAL_get_blockdiags(NODES,ilevel,flag_pot)

kvec      = zeros(1,size(NODES,2));
nvec      = zeros(1,size(NODES,2));
levellist = zeros(1,size(NODES,2));
nlevel    = 0;

for ibox = 2:size(NODES,2)
  if (NODES{2,ibox} == ilevel)
    nlevel            = nlevel+1;
    nvec(nlevel)      = NODES{8,ibox};
    kvec(nlevel)      = NODES{9,ibox};
    levellist(nlevel) = ibox;
  end
end
kvec      = kvec(1:nlevel);
nvec      = nvec(1:nlevel);
levellist = levellist(1:nlevel);

%mn = max(nvec);
%mk = max(kvec);
%DD = spalloc(sum(nvec), sum(nvec), mn*mn*nlevel);
%UU = spalloc(sum(nvec), sum(kvec), mn*mk*nlevel);
%VV = spalloc(sum(nvec), sum(kvec), mn*mk*nlevel);

nloc = sum(nvec);
kloc = sum(kvec);

IJDD = nan(sum(nvec.^2), 2);
IJUV = nan(sum(nvec.*kvec), 2);

% determine if value is complex or not
if contains(flag_pot, 'h')
    VALDD = zeros(sum(nvec.^2), 1, 'like', 1j);
    VALUV = zeros(sum(nvec.*kvec), 2, 'like', 1j);
else
    VALDD = zeros(sum(nvec.^2), 1);
    VALUV = zeros(sum(nvec.*kvec), 2);
end

ofs1 = 0;
ofs2 = 0;
for i = 1:nlevel
  ibox               = levellist(i);
  indk               = sum(kvec(1:(i-1))) + (1:kvec(i));
  indn               = sum(nvec(1:(i-1))) + (1:nvec(i));
  n                  = NODES{8,ibox};
  k                  = NODES{9,ibox};
  Jout               = NODES{31,ibox};
  Jin                = NODES{33,ibox};
  V                  = zeros(n,k);
  V(Jout(1:k),    :) = eye(k);
  V(Jout((k+1):n),:) = NODES{30,ibox}';
  U                  = zeros(n,k);
  U(Jin(1:k),    :)  = eye(k);
  U(Jin((k+1):n),:)  = NODES{32,ibox}';
  %UU(indn,indk)      = U;
  %VV(indn,indk)      = V;
  I = indn' * ones(1,k);
  J = ones(n,1) * indk;
  IJUV(ofs1+(1:n*k), 1) = I(:);
  IJUV(ofs1+(1:n*k), 2) = J(:);
  VALUV(ofs1+(1:n*k), 1) = U(:);
  VALUV(ofs1+(1:n*k), 2) = V(:);
  ofs1 = ofs1 + n*k;
  
  if (NODES{5,ibox}==0) % ibox is a leaf.
    %DD(indn,indn) = NODES{40,ibox};
    D = NODES{40,ibox};
  else
    ison1 = NODES{4,ibox}(1);
    ison2 = NODES{4,ibox}(2);
    k1    = NODES{9,ison1};
    k2    = NODES{9,ison2};
    D     = [zeros(k1,k1),NODES{46,ison1};...
                    NODES{46,ison2},zeros(k2,k2)];
  end
    I = indn' * ones(1,n);
    J = ones(n,1) * indn;
    IJDD(ofs2+(1:n^2), 1) = I(:);
    IJDD(ofs2+(1:n^2), 2) = J(:);
    VALDD(ofs2+(1:n^2)) = D(:);
    ofs2 = ofs2 + n^2;
end

assert(ofs1 == size(IJUV,1))
assert(ofs2 == size(IJDD,1))

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function M = LOCAL_get_blockdiags_memory(NODES,ilevel)

kvec      = zeros(1,size(NODES,2));
nvec      = zeros(1,size(NODES,2));
nlevel    = 0;

for ibox = 2:size(NODES,2)
  if (NODES{2,ibox} == ilevel)
    nlevel            = nlevel+1;
    nvec(nlevel)      = NODES{8,ibox};
    kvec(nlevel)      = NODES{9,ibox};
  end
end
kvec      = kvec(1:nlevel);
nvec      = nvec(1:nlevel);

M = sum(nvec.^2) + 2 * sum(nvec.*kvec) + 2 * sum(kvec);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function AT = LOCAL_get_AT(NODES,ilevel,C,flag_pot,params)

kvec      = zeros(1,size(NODES,2));
levellist = zeros(1,size(NODES,2));
nlevel    = 0;

for ibox = 2:size(NODES,2)
  if (NODES{2,ibox} == ilevel)
    nlevel            = nlevel+1;
    kvec(nlevel)      = NODES{9,ibox};
    levellist(nlevel) = ibox;
  end
end
kvec      = kvec(1:nlevel);
levellist = levellist(1:nlevel);
AT        = zeros(sum(kvec));

for i = 1:nlevel
  ibox = levellist(i);
  indi = sum(kvec(1:(i-1))) + (1:kvec(i));
  for j = [1:(i-1),(i+1):nlevel]
    jbox = levellist(j);
    indj = sum(kvec(1:(j-1))) + (1:kvec(j));
%    AT(indi,indj) = A(NODES{22,ibox},NODES{20,jbox});
    AT(indi,indj) = LOCAL_construct_A_offd(C,NODES{22,ibox},NODES{20,jbox},flag_pot,params);
  end
end

return
