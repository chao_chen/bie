function write_matrix_single(b, fname)
    [N,nrhs] = size(b);
    
%     display(N)
%     display(nrhs)
%     display(b(1:3,:), 'b')
    
    fid = fopen(fname,'w');
    fwrite(fid, N, 'integer*4');
    fwrite(fid, nrhs, 'integer*4');
    fwrite(fid, single(b), 'single');
    fclose(fid);
    
return