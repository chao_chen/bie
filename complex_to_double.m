function AA = complex_to_double(A)
R = real(A);
I = imag(A);
R = reshape(R,[1,numel(R)]);
I = reshape(I,[1,numel(I)]);
AA = [R;I];
AA = AA(:);
return