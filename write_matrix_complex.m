function write_matrix_complex(b, fname)
    [N,nrhs] = size(b);
    
%     display(N)
%     display(nrhs)
%     display(b(1:3,:), 'b')
    
    bb = complex_to_single(b);
    
%     display(bb(1:3), 'bb')
    
    fid = fopen(fname,'w');
    fwrite(fid, N, 'integer*4');
    fwrite(fid, nrhs, 'integer*4');
    fwrite(fid, bb, 'single');
    fclose(fid);
    
return