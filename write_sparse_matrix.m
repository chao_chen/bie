function write_sparse_matrix(A, fname)

[I, J, S] = find(A);

N = size(A,1);
H = histcounts(J,0:(N+1)); % 0-based indexing
C = cumsum(H);

fid = fopen(fname,'w');
fwrite(fid, N, 'integer*4');
fwrite(fid, nnz(A), 'integer*4');
fwrite(fid, C, 'integer*4');
fwrite(fid, I-1, 'integer*4'); % 0-based indexing

if isreal(AAA)
    fwrite(fid, S, 'double');
else
    SS = complex_to_single(S);
    fwrite(fid, SS, 'double');
end

fclose(fid);

display(N, 'N')
display(nnz(A), 'NNZ')

return