
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The factorization phase of the above bottom-up solver for HODLR matrix.
%
% It computes a factorization of a given HODLR matrix A, and the
% factorization can be used to solve a given right-hand-side B.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [YMAT, KMAT, FLOPS] = HODLR_factorize(NODES)

nboxes = size(NODES,2);
ntot   = NODES{7,1};
L      = NODES{2,nboxes};

cs = zeros(1, L+1);
cs(1) = 1; % the first level starts at the first column

ilevel = 1;
for ibox = 2:nboxes
    if NODES{2,ibox} == ilevel
        k = size(NODES{15,ibox},2);
        cs(ilevel+1) = cs(ilevel) + k;
        ilevel = ilevel + 1;
    end
end

%%% Build the matrix BMAT
%%% This is the large matrix that holds all W matrices stacked together
%%% (without right-hand side).
if isreal(NODES{15,2})
    YMAT = zeros(ntot, cs(L+1)-1);
else
    YMAT = zeros(ntot, cs(L+1)-1, 'like', 1j);
end

for ibox = 2:nboxes
  ilevel    = NODES{2,ibox};
  I         = NODES{6,ibox} - 1 + (1:NODES{7,ibox});
  J         = cs(ilevel) : (cs(ilevel+1)-1);
  YMAT(I,J) = NODES{15,ibox};
end

FLOP = 0;
KMAT = cell(3, nboxes);
%%% Execute the factorization
for ibox = size(NODES,2):(-1):1

  if (NODES{5,ibox} == 0) %%% Solve by brute force at the leaf nodes:
      
    [LL, UU, p] = lu(NODES{10,ibox}, 'vec');  
    I         = NODES{6,ibox} - 1 + (1:NODES{7,ibox});
    YMAT(I,:) = UU\(LL\YMAT(I(p),:));
    
    n = length(I);
    FLOP = FLOP + 2/3*n^3 + 2*n^2*size(YMAT,2);
  else %%% Parent box solve:
    
    %%% Extract stuff from the data structure and build index vectors.
    %%% J1 points into BMAT to mark the W-matrices at the current level.
    %%% J2 points into BMAT to mark what was carried down from higher levels.
    ison1   = NODES{4,ibox}(1);
    ison2   = NODES{4,ibox}(2);
    ilevel  = NODES{2,ison1};
    k       = size(NODES{15,ison1},2);
    V1      = NODES{12,ison1};
    V2      = NODES{12,ison2};
    I1      = NODES{6,ison1} - 1 + (1:NODES{7,ison1});
    I2      = NODES{6,ison2} - 1 + (1:NODES{7,ison2});
    J1      = cs(ilevel) : (cs(ilevel+1)-1);
    J2      = 1:(cs(ilevel)-1);
    %J1      = (L-ilevel-1)*k + (1:k);
    %J2      = ((L-ilevel)*k + 1):size(BMAT,2);
    
    %%% Execute the small local solve, then project back:
%    W12H    = BMAT(I1,J1);
%    B1H     = BMAT(I1,J2);
%    W21H    = BMAT(I2,J1);
%    B2H     = BMAT(I2,J2);
%    XH      = [eye(k),V1'*W12H;V2'*W21H,eye(k)]\[V1'*B1H;V2'*B2H];
%    BMAT(I1,J2) = B1H - W12H*XH(k + (1:k),:);
%    BMAT(I2,J2) = B2H - W21H*XH(     1:k ,:);

    [LL, UU, p] = lu([eye(k),V1'*YMAT(I1,J1);V2'*YMAT(I2,J1),eye(k)], 'vec');
    b = [V1'*YMAT(I1,J2);V2'*YMAT(I2,J2)];
    XH = UU\(LL\b(p,:));
    YMAT(I1,J2) = YMAT(I1,J2) - YMAT(I1,J1)*XH(k + (1:k),:);
    YMAT(I2,J2) = YMAT(I2,J2) - YMAT(I2,J1)*XH(     1:k ,:);
    
    FLOP = FLOP + 2/3*(2*k)^3 + 2*(2*k)^2*length(J2) + ...
        2*(numel(V1)+numel(V2))*length(J2) + ...
        2*(numel(YMAT(I1,J1))+numel(YMAT(I2,J1)))*length(J2);
  end
  
  KMAT{1,ibox} = LL;
  KMAT{2,ibox} = UU;
  KMAT{3,ibox} = p;
  
  if nargout > 2
      if isreal(YMAT)
          FLOPS = FLOP;
      else
          FLOPS = 4*FLOP;
      end
  end
end

return

