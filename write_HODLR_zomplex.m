function write_HODLR_zomplex(NODES, fname)
    nboxes = size(NODES,2);
    N = NODES{7,1};
    m = NODES{7,nboxes};
    L = NODES{2,nboxes};
   
    ranks = zeros(1, L);
    cs = zeros(1, L+1);
    cs(1) = 1; % the first level starts at the first column
    
    ilevel = 1;
    for ibox = 2:nboxes
        if NODES{2,ibox} == ilevel
            k = size(NODES{15,ibox},2);
            cs(ilevel+1) = cs(ilevel) + k;
            ranks(ilevel) = k;
            ilevel = ilevel + 1;
        end
    end
    ncol = cs(L+1)-1;
    
    %%% Build the matrix Ubig, Vbig, and Dbig
    U = zeros(N, ncol);
    V = zeros(N, ncol);
    D = zeros(m, N);
    for ibox = 2:nboxes
      ilevel    = NODES{2,ibox};
      I         = NODES{6,ibox} - 1 + (1:NODES{7,ibox});
      J         = cs(ilevel) : (cs(ilevel+1)-1);
      U(I,J)    = NODES{15,ibox};
      V(I,J)    = NODES{12,ibox};
      
      if (NODES{5,ibox} == 0) %%% leaf nodes
          I = NODES{6,ibox} - 1 + (1:NODES{7,ibox});
          D(:,I) = NODES{10,ibox};
      end
    end
    
%     display(N)
%     display(m)
%     display(L)
%     display(ranks, 'ranks')
%     display(U(1:3,1:3), 'U')
%     display(V(1:3,1:3), 'V')
%     display(D(1:3,1:3), 'D')
    
    UU = complex_to_double(U);
    VV = complex_to_double(V);
    DD = complex_to_double(D);
        
%     display(UU(1:3), 'UU')
%     display(VV(1:3), 'VV')
%     display(DD(1:3), 'DD')
        
    fid = fopen(fname,'w');
    fwrite(fid, N, 'integer*4');
    fwrite(fid, m, 'integer*4');
    fwrite(fid, L, 'integer*4');
    fwrite(fid, ranks, 'integer*4');
    fwrite(fid, UU, 'double');
    fwrite(fid, VV, 'double');
    fwrite(fid, DD, 'double');
    fclose(fid);
    
return


