function AA = complex_to_single(A)
R = single(real(A));
I = single(imag(A));
R = reshape(R,[1,numel(R)]);
I = reshape(I,[1,numel(I)]);
AA = [R;I];
AA = AA(:);
return