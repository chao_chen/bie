function write_matrix_zomplex(b, fname)
    [N,nrhs] = size(b);
    
%     display(N)
%     display(nrhs)
%     display(b(1:3,:), 'b')
    
    bb = complex_to_double(b);
    
%     display(bb(1:3), 'bb')
    
    fid = fopen(fname,'w');
    fwrite(fid, N, 'integer*4');
    fwrite(fid, nrhs, 'integer*4');
    fwrite(fid, bb, 'double');
    fclose(fid);
    
return