function NODES = HBS_to_HODLR_single(NODES)

nboxes = size(NODES,2);
L      = NODES{2,nboxes};

for ibox = nboxes:(-1):2
  ilevel = NODES{2,ibox};
  if (ilevel < L)
    assert(NODES{5,ibox}==2)
    ison1 = NODES{4,ibox}(1);
    ison2 = NODES{4,ibox}(2);
    
    Tout = NODES{30,ibox};
    Jout = NODES{31,ibox};
    Tin = NODES{32,ibox};
    Jin = NODES{33,ibox};
    
    n = NODES{8,ibox};
    k = NODES{9,ibox};
    V = zeros(n, k);
    U = zeros(n, k);
    
    V(Jout(1:k),:) = eye(k);
    V(Jout((k+1):end),:) = Tout';
    U(Jin(1:k),:) = eye(k);
    U(Jin((k+1):end),:) = Tin';    

    V1 = NODES{12,ison1};
    V2 = NODES{12,ison2};

    U1 = NODES{11,ison1};
    U2 = NODES{11,ison2};
    
    k1 = NODES{ 9,ison1};
%     NODES{12,ibox} = single([V1*V(1:k1,:); V2*V((k1+1):end,:)]);
%     NODES{11,ibox} = single([U1*U(1:k1,:); U2*U((k1+1):end,:)]);
%     NODES{15,ibox} = single(NODES{11,ibox}*NODES{46,ibox});
    
    NODES{12,ibox} = double(single([V1*V(1:k1,:); V2*V((k1+1):end,:)]));
    NODES{11,ibox} = double(single([U1*U(1:k1,:); U2*U((k1+1):end,:)]));
    NODES{15,ibox} = double(single(NODES{11,ibox}*NODES{46,ibox}));
    
  else
    assert(NODES{5,ibox}==0)
    NODES{10,ibox} = NODES{40,ibox};
    
    Tout = NODES{30,ibox};
    Jout = NODES{31,ibox};
    Tin = NODES{32,ibox};
    Jin = NODES{33,ibox};
    
    n = NODES{8,ibox};
    k = NODES{9,ibox};
    V = zeros(n, k);
    U = zeros(n, k);
    
    V(Jout(1:k),:) = eye(k);
    V(Jout((k+1):end),:) = Tout';
    U(Jin(1:k),:) = eye(k);
    U(Jin((k+1):end),:) = Tin';    
    
%     NODES{12,ibox} = single(V);
%     NODES{11,ibox} = single(U);
%     NODES{15,ibox} = single(U*NODES{46,ibox});
        
    NODES{12,ibox} = double(single(V));
    NODES{11,ibox} = double(single(U));
    NODES{15,ibox} = double(single(U*NODES{46,ibox}));
  end
end
end