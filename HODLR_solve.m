%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The solve/apply phase of the previous bottom-up solver for HODLR matrix.
%
% It reuses the factorization of a given HODLR matrix A to solve a given 
% right-hand-side vector B.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [B,FLOPS] = HODLR_solve(BMAT,KAMT,NODES,B)

nboxes = size(NODES,2);
L      = NODES{2,nboxes};

cs = zeros(1, L+1);
cs(1) = 1; % the first level starts at the first column

ilevel = 1;
for ibox = 2:nboxes
    if NODES{2,ibox} == ilevel
        k = size(NODES{15,ibox},2);
        cs(ilevel+1) = cs(ilevel) + k;
        ilevel = ilevel + 1;
    end
end

%%% Execute the actual solve
for ibox = size(NODES,2):(-1):1
    
  LL     = KAMT{1,ibox};
  UU     = KAMT{2,ibox};
  p      = KAMT{3,ibox};
  
  if (NODES{5,ibox} == 0) %%% Solve by brute force at the leaf nodes:
      
    I      = NODES{6,ibox} - 1 + (1:NODES{7,ibox});
    B(I,:) = UU\(LL\B(I(p),:));
    
  else %%% Parent box solve:
    
    %%% Extract stuff from the data structure and build index vectors.
    %%% J1 points into BMAT to mark the W-matrices at the current level.
    %%% J2 points into BMAT to mark what was carried down from higher levels.
    ison1   = NODES{4,ibox}(1);
    ison2   = NODES{4,ibox}(2);
    ilevel  = NODES{2,ison1};
    k       = size(NODES{15,ison1},2);
    V1      = NODES{12,ison1};
    V2      = NODES{12,ison2};
    I1      = NODES{6,ison1} - 1 + (1:NODES{7,ison1});
    I2      = NODES{6,ison2} - 1 + (1:NODES{7,ison2});
    J1      = cs(ilevel) : (cs(ilevel+1)-1);

%   (J2 is no longer needed)
%   J2      = ((L-ilevel)*k + 1):size(BMAT,2);
    
    %%% Execute the small local solve, then project back:
%    W12H    = BMAT(I1,J1);
%    B1H     = BMAT(I1,J2);
%    W21H    = BMAT(I2,J1);
%    B2H     = BMAT(I2,J2);
%    XH      = [eye(k),V1'*W12H;V2'*W21H,eye(k)]\[V1'*B1H;V2'*B2H];
%    BMAT(I1,J2) = B1H - W12H*XH(k + (1:k),:);
%    BMAT(I2,J2) = B2H - W21H*XH(     1:k ,:);

%   (Only the right-hand side vector 'B' is updated.)
%   (We can also precompute the 2k x 2k matrix in the factorization.)
    RHS     = [V1'*B(I1,:);V2'*B(I2,:)];
    XH      = UU\(LL\RHS(p,:));
    B(I1,:) = B(I1,:) - BMAT(I1,J1)*XH(k + (1:k),:);
    B(I2,:) = B(I2,:) - BMAT(I2,J1)*XH(     1:k ,:);
  end
end

%%% Gather the solution from the rightmost columns of BMAT.
%X = B;

if nargout > 1
      N = size(B,1);
      n = N/(2^L);
      FLOP = 2*n*N*size(B,2); % leaf level
      for i=1:L-1
          r = cs(i+1)-cs(i);
          FLOP = FLOP + 4*N*r*size(B,2) + (2*r)^2*size(B,2)*(2^i);
      end
      if isreal(BMAT)
          FLOPS = FLOP;
      else
          FLOPS = 4*FLOP;
      end
end
return